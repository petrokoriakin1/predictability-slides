# frozen_string_literal: true

module DataFetcher
  class Invesments
    def raw_data
      `#{command}`.split("\n")
    end

    def current_week
      parsed_data[Date.today.cweek]
    end

    def parsed_data
      raw_data.each_with_object(Hash.new(0)) do |log_entry, result|
        result[parse_log_time(log_entry).cweek] += parse_converted_value(log_entry)
      end
    end

    private

    def parse_converted_value(log_entry)
      log_entry.split(" invested €").last.to_i
    end

    def parse_log_time(log_entry)
      DateTime.parse(log_entry.match(/(?<=\().+?(?=\))/)[0])
    end

    def command
      <<~SLOT_CMD
        git --git-dir ../../worklog/.git log --format="%an (%ad): %s" --reverse --grep "invested"
      SLOT_CMD
    end
  end
end
