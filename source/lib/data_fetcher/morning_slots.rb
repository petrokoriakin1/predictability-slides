# frozen_string_literal: true

module DataFetcher
  class MorningSlots
    def raw_data
      `#{command}`.split("--\n")
    end

    def current_week
      parsed_data[Date.today.cweek]
    end

    def parsed_data
      values = raw_data.map { |i| i.split("\n") }
        .select { |tuple| valid?(tuple) }
        .map { |tuple| parse_finish_time(tuple) }
        .select { |finish_time| last_seven_weeks?(finish_time) && morning_time?(finish_time) }
        .group_by(&:cweek)
      (0..Date.today.cweek).each_with_object(Hash.new(0)) do |week, hash|
        hash[week] = values.fetch(week, []).size
      end
    end

    private

    def morning_time?(finish_timestamp)
      return true if finish_timestamp.hour < 10
      return true if finish_timestamp.hour == 10 && finish_timestamp.minute <= 30

      false
    end

    def last_seven_weeks?(finish_timestamp)
      finish_timestamp.cweek >=  (Date.today.cweek - 8)
    end

    def parse_finish_time(tuple)
      raw_date = tuple.last.split("(").last.split(")").first
      DateTime.parse(raw_date)
    end

    def valid?(tuple)
      return false unless tuple.size == 3
      return false unless tuple.first.match?("start focused")
      return false unless tuple[1].match?("slot result")
      return false if tuple[1].match?("slot result: 0")
      return false unless tuple.last.match?("finish focused")

      true
    end

    def command
      <<~SLOT_CMD
        git --git-dir ../../worklog/.git log --format="%an (%ad): %s" --reverse | grep "start focused slot" -A 2
      SLOT_CMD
    end
  end
end
