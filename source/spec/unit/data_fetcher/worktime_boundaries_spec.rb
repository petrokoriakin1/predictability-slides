# frozen_string_literal: true

require "spec_helper"

describe DataFetcher::WorktimeBoundaries do
  it "is a class" do
    expect(described_class).to be_a(Class)
  end

  describe "#current_week" do
    subject(:current_week) { described_class.new.current_week }

    it "returns integer" do
      expect(current_week).to be_an(Integer)
    end
  end

  describe "#parsed_data" do
    subject(:parsed_data) { described_class.new.parsed_data }

    it "returns hash" do
      expect(parsed_data).to be_an(Hash)
    end

    it "returns a hash with data" do
      expect(parsed_data.slice(28, 27, 26, 25, 23)).to eq({ 28 => 3, 27 => 5, 26 => 2, 25 => 2, 23 => 3 })
    end
  end
end
